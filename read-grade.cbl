       IDENTIFICATION DIVISION. 
       PROGRAM-ID. READ-GRADE-TO-AVG.
       AUTHOR. AREEYEO.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "mygrade.txt"
           ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-FILE ASSIGN TO "avg.txt"
           ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  GRADE-FILE.
       01  GRADE-DETAIL.
           88 END-OF-MYGRADE-FILE   VALUE  HIGH-VALUE.
           05 SBJ-ID         PIC   X(6).
           05 SBJ-NAME       PIC   X(50).
           05 CREDIT         PIC   9.
           05 GRADE          PIC   X(2).

       FD  AVG-FILE.
       01  AVG-DETAIL.
           05 AVG-GRADE      PIC 9V9(3).
           05 AVG-SCI-GRADE  PIC 9V9(3).
           05 AVG-CS-GRADE   PIC 9V9(3).
       WORKING-STORAGE SECTION. 
       01  SUM-CREDIT        PIC 9(3)V9(3).
       01  SUM-GRADE         PIC 9(3)V9(3).
       01  GRADE-NUM         PIC 9(3)V9(3).
       01  NUM-01            PIC X.
       01  NUM-02            PIC X(2).
       01  NUM-SCI-CREDIT    PIC 9(3)V9(3).
       01  NUM-SCI-GRADE     PIC 9(3)V9(3).
       01  NUM-CS-CREDIT     PIC 9(3)V9(3).
       01  NUM-CS-GRADE      PIC 9(3)V9(3).



       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT GRADE-FILE 
           OPEN OUTPUT AVG-FILE 
           PERFORM UNTIL END-OF-MYGRADE-FILE 
              READ GRADE-FILE 
                 AT END SET END-OF-MYGRADE-FILE TO TRUE
              END-READ
              IF NOT END-OF-MYGRADE-FILE THEN
                MOVE SBJ-ID TO NUM-01
                MOVE SBJ-ID TO NUM-02 
                PERFORM 001-PROCESS THRU 001-EXIT
              END-IF            
           COMPUTE AVG-GRADE      = SUM-GRADE / SUM-CREDIT
           COMPUTE AVG-SCI-GRADE  = NUM-SCI-GRADE  / NUM-SCI-CREDIT 
           COMPUTE AVG-CS-GRADE   = NUM-CS-GRADE  / NUM-CS-CREDIT 

           END-PERFORM
           DISPLAY "AVG-GRADE: " AVG-GRADE
           DISPLAY "AVG-SCI-GRADE: " AVG-SCI-GRADE 
           DISPLAY "AVG-CS-GRADE: " AVG-cs-GRADE          
           WRITE AVG-DETAIL 
           CLOSE GRADE-FILE 
           CLOSE AVG-FILE 
           GOBACK 
           .
       001-PROCESS.
           EVALUATE GRADE
              WHEN "A"  MOVE 4   TO  GRADE-NUM
              WHEN "B+" MOVE 3.5 TO  GRADE-NUM
              WHEN "B"  MOVE 3   TO  GRADE-NUM
              WHEN "C+" MOVE 2.5 TO  GRADE-NUM
              WHEN "C"  MOVE 2   TO  GRADE-NUM 
              WHEN "D+" MOVE 1.5 TO  GRADE-NUM
              WHEN "D"  MOVE 1   TO  GRADE-NUM
           END-EVALUATE 
           COMPUTE SUM-CREDIT = SUM-CREDIT + CREDIT  
           COMPUTE SUM-GRADE = SUM-GRADE + (GRADE-NUM * CREDIT)
           IF NUM-01 EQUAL 3 THEN 
              COMPUTE NUM-SCI-CREDIT = NUM-SCI-CREDIT + CREDIT  
              COMPUTE NUM-SCI-GRADE  = NUM-SCI-GRADE  + (GRADE-NUM * 
              CREDIT)
           END-IF
           IF NUM-02 EQUAL 31 THEN
              COMPUTE NUM-CS-CREDIT = NUM-CS-CREDIT + CREDIT  
              COMPUTE NUM-CS-GRADE  = NUM-CS-GRADE  + (GRADE-NUM * 
                 CREDIT)
           END-IF       
           .
       001-EXIT.
           EXIT.
       