       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-SCORE.
       AUTHOR. AREEYEO.
       
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL.
           SELECT SCORE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  SCORE-FILE.
       01  SCORE-DETAIL.
           88 END-OF-MYGRADE-FILE   VALUE  HIGH-VALUE.
           05 SBJ-ID         PIC   X(6).
           05 SBJ-NAME       PIC   X(50).
           05 CREDIT         PIC   x(1).
           05 GRADE          PIC   X(2).

       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT SCORE-FILE 
           MOVE  "202101"  TO SBJ-ID
           MOVE "Information Services and Study Fundamentals" 
           TO SBJ-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "B+"  TO GRADE
           WRITE SCORE-DETAIL
           
           *>/212101English I                                         3C
           MOVE  "212101"  TO SBJ-ID
           MOVE "English I" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE SCORE-DETAIL
           
           *>/302111Calculus and Analytic Geometry I                  3C
           MOVE  "302111"  TO SBJ-ID
           MOVE "Calculus and Analytic Geometry I" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE SCORE-DETAIL
           
           *>/303101Chemistry I                                       3C+
           MOVE  "303101"  TO SBJ-ID
           MOVE "Chemistry I" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE SCORE-DETAIL
           
           *>/303102Chemistry Laboratory I                            1A
           MOVE  "303102"  TO SBJ-ID
           MOVE "Chemistry Laboratory I" TO SBJ-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE SCORE-DETAIL

           *>/306101General Biology I                                 3C+
           MOVE  "306101"  TO SBJ-ID
           MOVE "General Biology I" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE SCORE-DETAIL

           *>/306102General Biology Laboratory I                      1C+
           MOVE  "306102"  TO SBJ-ID
           MOVE "General Biology Laboratory I" TO SBJ-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE SCORE-DETAIL

           *>/308101Introductory Physics I                            3C
           MOVE  "308101"  TO SBJ-ID
           MOVE "Introductory Physics I " TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE SCORE-DETAIL

           *>/308102Introductory Physics Laboratory I                 1B
           MOVE  "308102"  TO SBJ-ID
           MOVE "Introductory Physics Laboratory I" TO SBJ-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE SCORE-DETAIL

           *>/212102English II                                        3B
           MOVE  "212102"  TO SBJ-ID
           MOVE "English II" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE SCORE-DETAIL

           *>/302112Calculus and Analytic Geometry II                 3B
           MOVE  "302112"  TO SBJ-ID
           MOVE "Calculus and Analytic Geometry II" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE SCORE-DETAIL

           *>/303103Chemistry II                                      3C
           MOVE  "303103"  TO SBJ-ID
           MOVE "Chemistry II" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE SCORE-DETAIL

           *>/303104Chemistry Laboratory II                           1B+
           MOVE  "303104"  TO SBJ-ID
           MOVE "Chemistry Laboratory II" TO SBJ-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "B+"  TO GRADE
           WRITE SCORE-DETAIL

           *>/306103General Biology II                                3C
           MOVE  "306103"  TO SBJ-ID
           MOVE "General Biology II" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE SCORE-DETAIL

           *>/306104General Biology Laboratory II                     1C+
           MOVE  "306104"  TO SBJ-ID
           MOVE "General Biology Laboratory II" TO SBJ-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE SCORE-DETAIL

           *>/308103Introductory Physics II                           3C
           MOVE  "308103"  TO SBJ-ID
           MOVE "Introductory Physics II" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE SCORE-DETAIL

           *>/308104Introductory Physics Laboratory II                1C+
           MOVE  "308104"  TO SBJ-ID
           MOVE "Introductory Physics Laboratory II" TO SBJ-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310201Computer and Data Processing                      3A
           MOVE  "310201"  TO SBJ-ID
           MOVE "Computer and Data Processing" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE SCORE-DETAIL

           *>/203100Man and Civilization                              2C
           MOVE  "203100"  TO SBJ-ID
           MOVE "Man and Civilization" TO SBJ-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE SCORE-DETAIL

           *>/212301Reading Laboratory I                              2C+
           MOVE  "212301"  TO SBJ-ID
           MOVE "Reading Laboratory I" TO SBJ-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE SCORE-DETAIL

           *>/226100Man and Economy                                   2C
           MOVE  "226100"  TO SBJ-ID
           MOVE "Man and Economy" TO SBJ-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE SCORE-DETAIL

           *>/302213Calculus and Analytic Geometry III                2D+
           MOVE  "302213"  TO SBJ-ID
           MOVE "Calculus and Analytic Geometry III" TO SBJ-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "D+"  TO GRADE
           WRITE SCORE-DETAIL

           *>/302323Linear Algebra I                                  3D
           MOVE  "302323"  TO SBJ-ID
           MOVE "Linear Algebra I" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "D"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310211Computer Science I                                3C+
           MOVE  "310211"  TO SBJ-ID
           MOVE "Computer Science I" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310212Discrete Mathematics                              3C
           MOVE  "310212"  TO SBJ-ID
           MOVE "Discrete Mathematics" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE SCORE-DETAIL

           *>/312201Elementary Statistics                             3B
           MOVE  "312201"  TO SBJ-ID
           MOVE "Elementary Statistics" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE SCORE-DETAIL

           *>/208101Thai I                                            2C+
           MOVE  "208101"  TO SBJ-ID
           MOVE "Thai I" TO SBJ-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE SCORE-DETAIL

           *>/212302Reading Laboratory II                             2C
           MOVE  "212302"  TO SBJ-ID
           MOVE "Reading Laboratory II" TO SBJ-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE SCORE-DETAIL

           *>/224101Man and Politics                                  2B
           MOVE  "224101"  TO SBJ-ID
           MOVE "Man and Politics" TO SBJ-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE SCORE-DETAIL

           *>/225100Man, Society and Culture                          2B
           MOVE  "225100"  TO SBJ-ID
           MOVE "Man, Society and Culture" TO SBJ-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE SCORE-DETAIL

           *>/302315Ordinary Differential Equations                   3C
           MOVE  "302315"  TO SBJ-ID
           MOVE "Ordinary Differential Equations" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310213Computer Science II                               3B+
           MOVE  "310213"  TO SBJ-ID
           MOVE "Computer Science II" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B+"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310231Computer Oganization and Assembly Language        3A
           MOVE  "310231"  TO SBJ-ID
           MOVE "Computer Oganization and Assembly Language" 
           TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310341Data Structure and Algorithms                     3B
           MOVE  "310341"  TO SBJ-ID
           MOVE "Data Structure and Algorithms" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE SCORE-DETAIL

           *>/441101Wellness Development                              2B
           MOVE  "441101"  TO SBJ-ID
           MOVE "Wellness Development" TO SBJ-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE SCORE-DETAIL

           *>/601101Man and Aesthetics                                2B
           MOVE  "601101"  TO SBJ-ID
           MOVE "Man and Aesthetics" TO SBJ-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE SCORE-DETAIL

           *>/226111Introduction to Economics                         2C
           MOVE  "226111"  TO SBJ-ID
           MOVE "Introduction to Economics" TO SBJ-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE SCORE-DETAIL

           *>/230416Production Management                             3D
           MOVE  "230416"  TO SBJ-ID
           MOVE "Production Management" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "D"  TO GRADE
           WRITE SCORE-DETAIL

           *>/308371Introduction to Electronics                       3C+
           MOVE  "308371"  TO SBJ-ID
           MOVE "Introduction to Electronics" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310332Computer Architecture                             3B
           MOVE  "310332"  TO SBJ-ID
           MOVE "Computer Architecture" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310444File Processing                                   3A
           MOVE  "310444"  TO SBJ-ID
           MOVE "File Processing" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310482Computer Graphic                                  3B
           MOVE  "310482"  TO SBJ-ID
           MOVE "Computer Graphic" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE SCORE-DETAIL

           *>/301301Quality Management                                1D
           MOVE  "301301"  TO SBJ-ID
           MOVE "Quality Management" TO SBJ-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "D"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310327Principles of Programming Languages               3A
           MOVE  "310327"  TO SBJ-ID
           MOVE "Principles of Programming Languages" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310342Database Design                                   3B
           MOVE  "310342"  TO SBJ-ID
           MOVE "Database Design" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310414Software Engineering                              3B
           MOVE  "310414"  TO SBJ-ID
           MOVE "Software Engineering" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310453Operating System                                  3A
           MOVE  "310453"  TO SBJ-ID
           MOVE "Operating System" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310461Data Communication                                3C+
           MOVE  "310461"  TO SBJ-ID
           MOVE "Data Communication" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE SCORE-DETAIL

           *>/441113Basketball I                                      1A
           MOVE  "441113"  TO SBJ-ID
           MOVE "Basketball I" TO SBJ-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310452Compiler Construction                             3B+
           MOVE  "310452"  TO SBJ-ID
           MOVE "Compiler Construction" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B+"  TO GRADE
           WRITE SCORE-DETAIL

           *>//310462Computer Networks                                 3A
           MOVE  "310462"  TO SBJ-ID
           MOVE "Computer Networks" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310481Systems Analysis and Design                       3C
           MOVE  "310481"  TO SBJ-ID
           MOVE "Systems Analysis and Design" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310484Introduction to Artificial Intelligence           3C
           MOVE  "310484"  TO SBJ-ID
           MOVE "Introduction to Artificial Intelligence" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310485Office Automation Management                      3C+
           MOVE  "310485"  TO SBJ-ID
           MOVE "Office Automation Management" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310492Computer Seminar                                  1B+
           MOVE  "310492"  TO SBJ-ID
           MOVE "Computer Seminar" TO SBJ-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "B+"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310417Selected Topics                                   3B
           MOVE  "310417"  TO SBJ-ID
           MOVE "Selected Topics" TO SBJ-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE SCORE-DETAIL

           *>/310491Projects in Computer Science                      2A
           MOVE  "310491"  TO SBJ-ID
           MOVE "Projects in Computer Science" TO SBJ-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE SCORE-DETAIL
           CLOSE SCORE-FILE
           GOBACK
           .

           
